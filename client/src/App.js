import React, { useState } from "react";
import Input from "./components/Input";
import Tasks from "./components/Tasks";

function App() {

  const [task, setTask] = useState(''); // form input
  const [tasks, setTasks] = useState([]); // tasks list

  // pattern for new task
  function createTask(task) {
    return {
      id: new Date().valueOf(),
      name: task,
      completed: false
    }
  }

  // form input text change
  function handleTaskChange(e) {
    setTask(e.target.value);
  }

  // press enter to submit task
  function handleTaskSubmit(e) {
    if (e.keyCode === 13) {
      let newTask = createTask(task);
      let newTasks = tasks;
      newTasks.unshift(newTask);
      setTasks(newTasks);
      setTask('');
    }
  }

  // complete/uncomplete selected task
  function handleTaskClick(id) {
    let newTasks = tasks.map(
      task => task.id === id ? { ...task, completed: !task.completed } : task
    );

    setTasks(newTasks);
  }

  // remove task from list
  function handleTaskDoubleClick(id) {
    let newTasks = tasks.filter(task => task.id !== id);

    setTasks(newTasks);
  }

  return (
    <div>
      <Input 
        task={task}
        handleTaskChange={handleTaskChange}
        handleTaskSubmit={handleTaskSubmit}
      />

      <Tasks 
        tasks={tasks}
        handleTaskClick={handleTaskClick}
        handleTaskDoubleClick={handleTaskDoubleClick}
      />
    </div>
  );
}

export default App;