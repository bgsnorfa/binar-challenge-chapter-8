import React from "react";

function Tasks({ tasks, handleTaskClick, handleTaskDoubleClick }) {
    return (
        <div>
            <ul>
                {tasks.map(task => {
                    return (
                        <li
                            key={task.id}
                            onClick={() => handleTaskClick(task.id)}
                            onDoubleClick={() => handleTaskDoubleClick(task.id)}
                            style={
                                {
                                    fontSize: '1.4em',
                                    textDecoration: task.completed ? 'line-through' : '',
                                    color: task.completed ? 'red' : '' 
                                }
                            }
                        >
                            {task.name}
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

export default Tasks;