import React from "react";

function Input({ task, handleTaskChange, handleTaskSubmit }) {
    return (
        <div>
            <input
                className="form-control"
                autoComplete="nope"
                type="text"
                placeholder="Input your task"
                value={task}
                onChange={handleTaskChange}
                onKeyDown={handleTaskSubmit}
            />
        </div>
    );
}

export default Input;